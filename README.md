# pushover.sh

Simple script to send notifications using bash/curl.
Also supports sending attachments (images).

## Configuration (optional)

Create a file `$HOME/.pushover.conf` containing the following data.
Alternatively you can supply these optins as command line arguments.

```
TOKEN="---your---api---token---"
USER="---your---user---token---"
CURL_OPTS=""
```

## Usage

```
./pushover.sh <options> <message>
 -c <callback>
 -d <device>
 -D <timestamp>
 -e <expire>
 -f <config_file>
 -p <priority>
 -r <retry>
 -t <title>
 -T <TOKEN> (required if not in config file)
 -m <msg_file>
 -i <attachment_file>
 -s <sound>
 -u <url>
 -U <USER> (required if not in config file)
 -a <url_title>
```

- Example (simple message): `./pushover.sh -t "Simple test" "This is a message"`
- Example (image attachment): `./pushover.sh -t "Image test" -i /path/to/snapshot.png "This is a message"`
- Example (msg from file content): `./pushover.sh -t "File test" -m /path/to/text.file`

## Forked

Forked from github: https://github.com/jnwatts/pushover.sh
Thanks to jnwatts for the initial version.